
var express = require('express'),
    favicon = require('serve-favicon'),
    jade = require('jade'),
    routes = require('./routes/index');
//URLS
var faviconURL = `${__dirname}/public/img/favicon.ico`,
    publicDir = express.static(`${__dirname}/public`),
    viewDir  = `${__dirname}/views`,
    port = (process.env.port || 3000),
    app = express();

app
    //configurando la app
    .set('views', viewDir)
    .set('view engine', 'jade')
    .set('port', port)
    //ejecutando middlewares
    .use( favicon(faviconURL) )
    .use(publicDir)
    .use('/', routes)

module.exports = app